package com.mitong.algorithms.unionfind;

/**
 * @author tong.mi
 * @email tong.mi@qunar.com
 * @date 2015/9/17
 * @description id数组的index代表集合中的元素，在同一集合中的元素的值相同，这样判断是否连接很快，但连接两个元素的操作需要便利整个数组
 */
public class QuickFindUF {
    private int[] id;

    public QuickFindUF(int N) {
        id = new int[N];
        for (int i = 0; i < N; i++) {
            id[i] = i;
        }
    }

    public boolean connected(int p, int q) {
        return id[p] == id[q];
    }

    public void union(int p, int q) {
        int pid = id[p];
        int qid = id[q];
        for (int i = 0; i < id.length; i++) {
            if (id[i] == pid) {
                id[i] = qid;
            }
        }
    }
}
