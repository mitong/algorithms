package com.mitong.algorithms.unionfind;

/**
 * @author tong.mi
 * @email tong.mi@qunar.com
 * @date 2015/9/17
 * @description 把每个集合看作一棵树，判断是否连接只需判断根是否相同，而连接两个节点只需将它们的根连在一起
 */
public class QuickUnionUF {
    private int[] id;

    public QuickUnionUF(int N) {
        id = new int[N];
        for (int i = 0; i < N; i++) {
            id[i] = i;
        }
    }

    /**
     * 找到某个节点的根节点
     * */
    private int root(int i) {
        while (i != id[i]) {
            i = id[i];
        }
        return i;
    }

    public boolean connected(int p, int q) {
        return root(p) == root(q);
    }

    public void union(int p, int q) {
        int i = root(p);
        int j = root(q);
        id[i] = j;
    }
}
